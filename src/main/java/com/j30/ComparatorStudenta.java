package com.j30;

import com.j30.collections.zadania.zadanie_5_student.Student;

import java.util.Comparator;

public class ComparatorStudenta implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {

//        return Integer.compare(o1.getAge(), o2.getAge());
        if (o1.getAge() > o2.getAge()) {
            return -1;
        } else if (o1.getAge() < o2.getAge()) {
            return 1;
        }

        return o1.getImie().compareTo(o2.getImie());
    }
}
