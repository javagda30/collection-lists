package com.j30.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Benchmark {
    public static void main(String[] args) {

        // todo: tutaj możesz zmienić
        //  implementację z linkedlist na arraylist
        List<Integer> list = new ArrayList<Integer>();
//        List<Integer> list = new LinkedList<Integer>();
        for (int i = 0; i < 100000; i++) {
            list.add(i);
        }

        for (int j = 0; j < 10; j++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long timeStart = System.currentTimeMillis(); // czas w milisekundach
            for (int i = 0; i < 100000; i++) {
                // todo: tutaj możesz zmienić operację i dokonać pomiaru czasu:
//                list.add(0, i);                     // dodaj na początek
//                list.add(i);                             // dodaj na koniec
                list.get((int) i);                       // pobierz - do pomiaru pobierz zmień pętle na: for (int i = 0; i < list.size(); i++) {
//                list.remove(list.size()-1);         // usuń ostatni element
//                list.remove(0);                     // usuń pierwszy element
            }
            long timeStop = System.currentTimeMillis(); // czas w milisekundach
            System.out.println(timeStop - timeStart);
        }
    }
}
