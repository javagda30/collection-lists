package com.j30.collections.zadania.zadanie_rachunek_i_produkty;

public class Main {
    public static void main(String[] args) {
        Produkt produkt1 = new Produkt("masło", 2.50, PodatekProduktu.VAT23);
        Produkt produkt2 = new Produkt("chleb", 2.40, PodatekProduktu.VAT8);
        Produkt produkt3 = new Produkt("szynka", 6.50, PodatekProduktu.VAT23);
        Produkt produkt4 = new Produkt("ser", 4.50, PodatekProduktu.VAT23);

        Rachunek rachunek = new Rachunek();

        rachunek.dodajProdukt(produkt1);
        rachunek.dodajProdukt(produkt2);
        rachunek.dodajProdukt(produkt3);
        rachunek.dodajProdukt(produkt4);

        rachunek.wypiszRachunek();


    }
}
