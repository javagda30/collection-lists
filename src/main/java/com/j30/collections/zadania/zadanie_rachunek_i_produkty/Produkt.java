package com.j30.collections.zadania.zadanie_rachunek_i_produkty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Produkt {
    private String nazwa;
    private Double netto;
    private PodatekProduktu podatekProduktu;

    public double podajCeneBrutto() {
        return netto * (1 + podatekProduktu.getPodatek()); // netto * (1.05)
//        return netto + (netto * podatekProduktu.getPodatek()); // to samo
    }

    @Override
    public String toString() {
        return "Produkt: " + nazwa + ", cena =" + netto + " * " + podatekProduktu;
    }
}
