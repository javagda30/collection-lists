package com.j30.collections.zadania.zadanie_rachunek_i_produkty;

import java.util.ArrayList;
import java.util.List;

public class Rachunek {
    private List<Produkt> produktList = new ArrayList<>();

    public void dodajProdukt(Produkt produkt) {
        produktList.add(produkt);
    }

    public void wypiszRachunek() {
        for (Produkt produkt : produktList) {
            System.out.println(produkt);
        }
    }

    public double podsumujRachunekNetto() {
        // sumujemy kwoty netto każdego produktu na liście
//        Todo: iteracyjnie
//        double suma = 0.0;
//        for (Produkt produkt : produktList) {
//            suma += produkt.getNetto();
//        }
//        return suma;

//      todo: streamem:
        return produktList.stream().mapToDouble(Produkt::getNetto).sum();
    }

    public double podsumujRachunekBrutto() {
        // sumujemy kwoty brutto każdego produktu na liście
//        Todo: iteracyjnie:
//        double suma = 0.0;
//        for (Produkt produkt : produktList) {
//            suma += produkt.podajCeneBrutto();
//        }
//        return suma;

//        todo: streamem:
        return produktList.stream().mapToDouble(Produkt::podajCeneBrutto).sum();
    }

    public double zwrocWartoscPodatku() {
        return podsumujRachunekBrutto() - podsumujRachunekNetto();
    }
}
