package com.j30.collections.zadania.zadanie_6_dziennik;

import com.j30.collections.zadania.zadanie_5_student.Plec;
import com.j30.collections.zadania.zadanie_5_student.Student;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Dziennik dziennik = new Dziennik();

//        System.out.println(new Student("123", "abc", "ijk", Plec.MEZCZYZNA, 15));

        dziennik.dodajStudenta(new Student("123", "abc", "ijk", Plec.MEZCZYZNA, 13));
        dziennik.dodajStudenta(new Student("234", "def", "gha", Plec.MEZCZYZNA, 99));
        dziennik.dodajStudenta(new Student("345", "gha", "def", Plec.MEZCZYZNA, 77));
        dziennik.dodajStudenta(new Student("123", "ijk", "abc", Plec.MEZCZYZNA, 1));
        dziennik.dodajStudenta(new Student("123", "abc", "abc", Plec.MEZCZYZNA, 15));
        dziennik.dodajStudenta(new Student("123r", "aad", "abc", Plec.MEZCZYZNA, 15));
        dziennik.dodajStudenta(new Student("123", "ijk", "abc", Plec.MEZCZYZNA, 26));
        dziennik.dodajStudenta(new Student("123", "ijk", "abc", Plec.MEZCZYZNA, 3));

        dziennik.listuSortowanych();

//        dziennik.usunStudenta("123");

        System.out.println(dziennik);

        Optional<Student> studentWPudelku = dziennik.zwrocStudenta("333");
        if (studentWPudelku.isPresent()) {
            Student student = studentWPudelku.get();

            System.out.println(student.getNrIndeksu());
        } else {
            System.out.println("Brak studenta.");
        }

        dziennik.dodajOcene("234", 5.0);
        dziennik.dodajOcene("234", 2.0);
        dziennik.dodajOcene("234", 3.0);
        dziennik.dodajOcene("234", 2.5);

        System.out.println(dziennik.podajSredniaStudenta("234"));
        System.out.println(dziennik.podajSredniaStudenta("333"));

    }
}
