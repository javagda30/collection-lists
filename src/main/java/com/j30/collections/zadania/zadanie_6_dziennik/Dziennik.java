package com.j30.collections.zadania.zadanie_6_dziennik;

import com.j30.ComparatorStudenta;
import com.j30.collections.zadania.zadanie_5_student.Student;

import java.util.*;

public class Dziennik {
    private List<Student> students = new ArrayList<>();

    public void dodajStudenta(Student student) {
        students.add(student);
    }

    public void usunStudenta(Student student) {
        students.remove(student);
    }

    public void usunStudenta(String indeksStudenta) { // O(n)
        // oddzielna lista z kopią elementów
        List<Student> kopiaListaIterowana = new ArrayList<>(students);

        for (Student student : kopiaListaIterowana) {
            if (student.getNrIndeksu().equalsIgnoreCase(indeksStudenta)) { // szukamy studenta po indeksie
                students.remove(student);
                break;
            }
        }
    }

    public Optional<Student> zwrocStudenta(String indeksStudenta) { // O(n)
        for (Student student : students) {
            if (student.getNrIndeksu().equalsIgnoreCase(indeksStudenta)) { // szukamy studenta po indeksie
                return Optional.of(student);
            }
        }
//        throw new NoSuchElementException();
        return Optional.empty();
    }

    public double podajSredniaStudenta(String indeksStudenta) {
        Optional<Student> studentOptional = zwrocStudenta(indeksStudenta);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            double suma = 0.0;
            for (Double aDouble : student.getOceny()) {
                suma += aDouble;
            }

            return suma / student.getOceny().size();
        }

        throw new NoSuchElementException("Brak wartości średniej. Nie znaleziono studenta.");
    }

    public void dodajOcene(String indeksStudenta, Double ocena) {
        Optional<Student> studentOptional = zwrocStudenta(indeksStudenta);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            student.getOceny().add(ocena);
        }else{
            System.err.println("Brak studenta o podanym id.");
        }
    }

    public void listuSortowanych(){
        Collections.sort(students, new ComparatorStudenta());
        for (Student student : students) {
            System.out.println(student);
        }

    }
}
