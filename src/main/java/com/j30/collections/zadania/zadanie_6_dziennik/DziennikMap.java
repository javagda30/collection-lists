package com.j30.collections.zadania.zadanie_6_dziennik;

import com.j30.collections.zadania.zadanie_5_student.Plec;
import com.j30.collections.zadania.zadanie_5_student.Student;

import java.util.*;

public class DziennikMap {
    private Map<String, Student> students = new HashMap<>();

    public void dodajStudenta(String indeks, String imie, String nazwisko, Plec plec){
        students.put(indeks, new Student(indeks, imie, nazwisko, plec, 0));
    }

    public void dodajStudenta(Student student) {
        if(students.containsKey(student.getNrIndeksu())){
            System.err.println("Usuwam stary wpis, nadpisuje go nowym.");
        }
        students.put(student.getNrIndeksu(), student);
    }

    public void usunStudenta(Student student) {
        students.remove(student.getNrIndeksu());
    }

    public void usunStudenta(String indeksStudenta) { // O(n)
        students.remove(indeksStudenta);
    }

    public Optional<Student> zwrocStudenta(String indeksStudenta) { // O(n)
        return Optional.ofNullable(students.get(indeksStudenta)); // (theta)(1)
    }

    public double podajSredniaStudenta(String indeksStudenta) {
        Optional<Student> studentOptional = zwrocStudenta(indeksStudenta);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            double suma = 0.0;
            for (Double aDouble : student.getOceny()) {
                suma += aDouble;
            }

            return suma / student.getOceny().size();
        }

        throw new NoSuchElementException("Brak wartości średniej. Nie znaleziono studenta.");
    }

    public void dodajOcene(String indeksStudenta, Double ocena) {
        Optional<Student> studentOptional = zwrocStudenta(indeksStudenta);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            student.getOceny().add(ocena);
        } else {
            System.err.println("Brak studenta o podanym id.");
        }
    }
}
