package com.j30.collections.zadania.zadanie_5_student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();

        students.addAll(Arrays.asList(
                new Student("123", "Pawel", "Gawel", Plec.MEZCZYZNA, 0),
                new Student("456", "Marian", "Kowalski", Plec.MEZCZYZNA, 0),
                new Student("45645", "Jarek", "Nowak", Plec.MEZCZYZNA, 0),
                new Student("567", "Olek", "Kohsin", Plec.MEZCZYZNA, 0),
                new Student("841", "Zosia", "Aawfsaeg", Plec.KOBIETA, 0),
                new Student("23187", "Gosia", "Niewiem", Plec.KOBIETA, 0),
                new Student("23187", "Tosia", "Tywiesz", Plec.KOBIETA, 0)
        ));

//        A:
        System.out.println("Zadanie 5a:");
        System.out.println(students);

//        B:
        System.out.println("Zadanie 5b:");
        for (Student student : students) {
            System.out.println(student);
        }

//        C:
        System.out.println("Zadanie 5c:");
        for (Student student : students) {
            if (student.getPlec() == Plec.KOBIETA) {
                System.out.println(student);
            }
        }

//        D:
        System.out.println("Zadanie 5d:");
        for (Student student : students) {
            if (student.getPlec() == Plec.MEZCZYZNA) {
                System.out.println(student);
            }
        }

//        E:
        System.out.println("Zadanie 5e:");
        for (Student student : students) {
            System.out.println(student.getNrIndeksu());
        }
    }
}
