package com.j30.collections.zadania.zadanie_5_student;


import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String nrIndeksu;
    private String imie;
    private String nazwisko;
    private Plec plec;
    private int age;
    private List<Double> oceny;

    public Student(String nrIndeksu, String imie, String nazwisko, Plec plec, int wiek) {
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
        this.age = wiek;
        this.oceny = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(nrIndeksu, student.nrIndeksu) &&
                Objects.equals(imie, student.imie) &&
                Objects.equals(nazwisko, student.nazwisko) &&
                plec == student.plec &&
                Objects.equals(oceny, student.oceny);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nrIndeksu, imie, nazwisko, plec, oceny);
    }
}
