package com.j30.collections.zadania.map_1;

import java.util.Map;
import java.util.TreeMap;

public class MainUniversity {
    public static void main(String[] args) {
        University university = new University();

        // a. put
        university.addStudent(123123L, "Abc", "Def");

        university.addStudent(100300L, "Xbc", "Kef");

        university.addStudent(100400L, "Ybc", "Jef");

        university.addStudent(100500L, "Zbc", "Hef");

        university.addStudent(100600L, "Wbc", "Eef");

        university.addStudent(100700L, "Nbc", "Fef");

        // b. containsKey
        if(university.containsStudent(100400)){
            System.out.println("Zawiera klucz 100400");
        }else{
            System.out.println("Nie zawiera klucza.");
        }

        // c. get
        System.out.println(university.getStudent(100400));

        // d. size
        System.out.println(university.studentsCount());

        // e. (pętla for each) -> values
        university.printAllStudents();

        // f. podmienić słowo HashMap na TreeMap przy deklaracji mapy

    }
}
