package com.j30.collections.zadania.map_1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private long indeks;
    private String imie;
    private String nazwisko;
}
