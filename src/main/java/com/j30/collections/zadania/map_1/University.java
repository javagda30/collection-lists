package com.j30.collections.zadania.map_1;

import java.util.HashMap;
import java.util.Map;

public class University {
    private Map<Long, Student> students = new HashMap<>();

    public void addStudent(long ind, String im, String nazw){
        Student nowyStudent = new Student(ind, im, nazw);
        students.put(ind, nowyStudent);
    }

    public boolean containsStudent(long indeksSzukany){
        return students.containsKey(indeksSzukany);
    }

    public Student getStudent(long indeks){
        return students.get(indeks);
    }

    public int studentsCount(){
        return students.size();
    }

    public void printAllStudents(){
        System.out.println("Pętla iterująca wartości:");
        for (Student value : students.values()) {
            System.out.println(value);
        }
    }

}
