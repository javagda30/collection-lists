package com.j30.collections.zadania.map_1;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
//        Student student =
//                new Student(100400L, "Paweł", "Gaweł");

//        Map<Long, Student> map = new HashMap<>();
        Map<Long, Student> map = new TreeMap<>();

        // a. put
        Student student_1 = new Student(123123L, "Abc", "Def");
        map.put(student_1.getIndeks(), student_1);

        Student student_2 = new Student(100300L, "Xbc", "Kef");
        map.put(student_2.getIndeks(), student_2);

        Student student_3 = new Student(100400L, "Ybc", "Jef");
        map.put(student_3.getIndeks(), student_3);

        Student student_4 = new Student(100500L, "Zbc", "Hef");
        map.put(student_4.getIndeks(), student_4);

        Student student_5 = new Student(100600L, "Wbc", "Eef");
        map.put(student_5.getIndeks(), student_5);

        Student student_6 = new Student(100700L, "Nbc", "Fef");
        map.put(student_6.getIndeks(), student_6);

        // b. containsKey
        if(map.containsKey(100400L)){ // Object
            System.out.println("Zawiera klucz 100400");
        }else{
            System.out.println("Nie zawiera klucza.");
        }

        // c. get
        System.out.println(map.get(100400L));
        System.out.println(map.get(100401L)); // null

        // d. size
        System.out.println(map.size());

        // e. (pętla for each) -> values
//        map.values() // zwraca wartości
//        map.keySet() // zwraca klucze
//        map.entrySet() // zbiór (set) wpisów [Entry] - wpis posiada klucz i przypisaną do niego wartość
        System.out.println("Pętla iterująca wartości:");
        for (Student value : map.values()) {
            System.out.println(value);
        }

        // f. podmienić słowo HashMap na TreeMap przy deklaracji mapy

    }
}
