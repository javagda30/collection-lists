package com.j30.collections.array;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        MyArrayList<Double> list = new MyArrayList<Double>(100);

//        list.add("abc");
//        list.add("def");
//        list.add(1);
        list.add(5.0);
        list.add(0.3);
        list.add(0.4);
        list.add(0.5);
        list.add(0.6);
        list.add(0.7); // to jest 8 element
        list.add(0.8);

        System.out.println(list);
        list.remove(8);     //0.7
        System.out.println(list);
        list.remove(8);         //0.8
        System.out.println(list);
        list.remove(0);                 // usun pierwszy
        System.out.println(list);
        list.remove(list.size() - 1);       // usun ostatni
        System.out.println(list);

        while (list.size() > 0){
            list.remove(0);
            System.out.println(list);
        }

        list.add(0, 1.0);
        list.add(0, 2.0);
        list.add(0, 3.0);
        list.add(0, 4.0);
        System.out.println(list);
        list.add(1, 4.0);
        System.out.println(list);
        list.add(1, 3.0);
        System.out.println(list);
        list.add(list.size(), 3.0);
        System.out.println(list);
    }
}
