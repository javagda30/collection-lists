package com.j30.collections.array;

public class MyArrayList<T> {
    private static final int INITIAL_ARRAY_SIZE = 10;

    private Object[] array;
    private int size = 0; // rozmiar listy, na początku 0

    public MyArrayList() {
        this(INITIAL_ARRAY_SIZE);
    }

    public MyArrayList(int initialArraySize) {
        array = new Object[initialArraySize];
    }

    // todo:
//      - dodanie elementu (na koniec)   ==
//      - listowanie elementów           == toString
//      - size                           ==
//      - usunięcie elementu z pozycji n ==
//      - dodanie elementu (na pozycję)

    public int size() {
        return size;
    }

    //    dodanie elementu (na koniec)
    public void add(T element) {
        checkSizeAndExtendIfNeeded(); // sprawdz rozmiar i rozszerz jeśli trzeba
        array[size++] = element;
        // post-inkrementacja -> najpierw wykona się linia, potem inkrementacja
    }

    public void add(int indeks, T element) {
        if (indeks < 0 || indeks > size) {
            throw new IndexOutOfBoundsException("Invalid index: " + indeks);
        }

        // metoda nie dojdzie do tego elementu jeśli wyjątek się rzuci,
        // dlatego nie ma sensu robić 'else' do poprzedniego ifa.

        if (size >= array.length) { // brakuje nam miejsca w tablicy
            // 1. stworzyć tablicę 2 x większą
            Object[] newArray = new Object[array.length * 2];
            for (int i = size; i > indeks; i--) {
                newArray[i] = array[i - 1]; // przepisuje następny w pole obecnego
            }
            newArray[indeks] = element;
            for (int i = 0; i < indeks; i++) {
                newArray[i] = array[i];
            }
            array = newArray;

        } else { // kiedy mamy dostatecznie dużo miejsca
            for (int i = size; i > indeks; i--) {
                array[i] = array[i - 1]; // przepisuje następny w pole obecnego
            }
            array[indeks] = element;
        }
        size++;
    }

    private void checkSizeAndExtendIfNeeded() {
        if (size >= array.length) {
            // 1. stworzyć tablicę 2 x większą
            Object[] newArray = new Object[array.length * 2];

            // 2. przepisujemy elementy
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
            }

            array = newArray; // nie interesuje mnie stara, tylko nowa.
        }
    }

    public T get(int n){
        return (T) array[n];
    }

    /**
     * Usuń element na pozycji.
     *
     * @param indeks - pozycja z której usuwamy
     */
    public void remove(int indeks) {
        if (indeks >= 0 && indeks < size) {
            for (int i = indeks; i < size - 1; i++) {
                array[i] = array[i + 1];
            }
//            size = size - 1;
            array[--size] = null; // pre dekrementacja
        } else {
            throw new IndexOutOfBoundsException("Invalid index: " + indeks);
        }
    }

    @Override
    public String toString() {
//        lista -> 1 2 3 4
//        [1, 2, 3, 4]
//        lista -> "abc" "def" "gha"
//        ["abc", "def", "gha"] << good

//        ["abc", "def", "gha", ] << bad
        StringBuilder sb = new StringBuilder("[");

        if (size > 0) {
            for (int i = 0; i < size; i++) {
                sb.append(array[i]);
                if (i != size - 1) { // pod warunkiem że nie jesteśmy na ostatnim elemencie
                    sb.append(", ");
                }
            }
        }
        sb.append("]");

        return sb.toString();
    }
}
