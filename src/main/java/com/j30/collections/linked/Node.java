package com.j30.collections.linked;

public class Node<Y> {
    private Y data;
    private Node<Y> prev; // poprzednik
    private Node<Y> next; // następnik

    public Node(Y data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }

    public Y getData() {
        return data;
    }

    public void setData(Y data) {
        this.data = data;
    }

    public Node<Y> getPrev() {
        return prev;
    }

    public void setPrev(Node<Y> prev) {
        this.prev = prev;
    }

    public Node<Y> getNext() {
        return next;
    }

    public void setNext(Node<Y> next) {
        this.next = next;
    }
}
