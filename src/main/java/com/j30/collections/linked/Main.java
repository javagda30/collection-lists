package com.j30.collections.linked;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(1);
        myLinkedList.add(2);
        myLinkedList.add(3);
        myLinkedList.add(4);
        myLinkedList.add(5);
        myLinkedList.add(6);
        System.out.println(myLinkedList);

        myLinkedList.remove(1);
        System.out.println(myLinkedList);
        myLinkedList.remove(3);
        System.out.println(myLinkedList);

        myLinkedList.removeFirst();
        System.out.println(myLinkedList);
        myLinkedList.removeLast();
        System.out.println(myLinkedList);
        myLinkedList.removeFirst();
        System.out.println(myLinkedList);
        myLinkedList.removeFirst();
        System.out.println(myLinkedList);
//        myLinkedList.removeFirst();

        // [ ] pusta lista

        myLinkedList.addFirst(5);
        System.out.println(myLinkedList);
        myLinkedList.addFirst(10);
        System.out.println(myLinkedList);
        myLinkedList.addLast(15);
        System.out.println(myLinkedList);
        myLinkedList.addLast(20);
        System.out.println(myLinkedList);

        System.out.println(myLinkedList.get(0));
        System.out.println(myLinkedList.get(1));
        System.out.println(myLinkedList.get(2));
        System.out.println(myLinkedList.get(3));
//        System.out.println(myLinkedList.get(4));
    }
}
