package com.j30.collections.linked;

public class MyLinkedList<T> {
    private Node<T> head;
    private Node<T> tail;

    private int size;

    public MyLinkedList() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public int size() {
        return size;
    }

    public void add(int indeks, T element) {
        if (indeks < 0 || indeks > size) {
            throw new IndexOutOfBoundsException();
        } else if (size == 0) {
            add(element);
        } else if (indeks == 0) {
            addFirst(element);
        } else if (indeks == size) {
            addLast(element);
        } else {
            Node<T> newNode = new Node<>(element);

            Node<T> tmp = head;
            int currentElementIndex = 0;
            while (indeks != currentElementIndex &&  // dopóki nie dotarliśmy do elementu o numerze 'indeks'
                    tmp != null) {                  // oraz dopóki mamy elementy
                currentElementIndex++;
                tmp = tmp.getNext();
            }

            Node<T> poprzednik = tmp.getPrev();
            Node<T> nastepnik = tmp;


            poprzednik.setNext(newNode);
            nastepnik.setPrev(newNode);
            newNode.setNext(nastepnik);
            newNode.setPrev(poprzednik);

            size++;
        }
    }

    public T get(int indeks) {
        if (indeks < 0 || indeks >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node<T> tmp = head;
        int currentElementIndex = 0;
        while (indeks != currentElementIndex &&  // dopóki nie dotarliśmy do elementu o numerze 'indeks'
                tmp != null) {                  // oraz dopóki mamy elementy
            currentElementIndex++;
            tmp = tmp.getNext();
        }

        return tmp.getData();
    }

    public void addFirst(T element) {
        if (size == 0) {
            add(element);
            return;
        }
        Node<T> node = new Node(element);

        head.setPrev(node);
        node.setNext(head);
        head = node;

        size++;
    }

    public void addLast(T element) {
        if (size == 0) {
            add(element);
            return;
        }
        Node<T> node = new Node(element);

        tail.setNext(node);
        node.setPrev(tail);
        tail = node;

        size++;
    }

    public void add(T element) {
        Node<T> node = new Node<>(element);

        if (size == 0 && head == null) { // nie ma elementów na liście
            head = node;
            tail = node;
        } else if (size > 0) { // size > 0
            tail.setNext(node);
            node.setPrev(tail);

            tail = node;
        }

        size++;
    }

    public void remove(int indeks) {
        if (size < 1) {
            throw new IndexOutOfBoundsException();
        }
        if (size == 1) {
            removeOnlyElement();

        } else if (indeks == 0) { //usun pierwszy
            removeFirst();
        } else if (indeks == size - 1) { // usun ostatni
            removeLast();
        } else { // else wycinamy coś z środka
            Node<T> tmp = head;
            int currentElementIndex = 0;
            while (indeks != currentElementIndex &&  // dopóki nie dotarliśmy do elementu o numerze 'indeks'
                    tmp != null) {                  // oraz dopóki mamy elementy
                currentElementIndex++;
                tmp = tmp.getNext();
            }

            Node<T> poprzednik = tmp.getPrev();
            Node<T> nastepnik = tmp.getNext();

            poprzednik.setNext(nastepnik);
            nastepnik.setPrev(poprzednik);

            size--;
        }
    }

    private void removeOnlyElement() {
        head = null;
        tail = null;

        size--;
    }

    public void removeLast() {
        if (size < 1) {
            throw new IndexOutOfBoundsException();
        }
        if (size == 1) {
            removeOnlyElement();
            return;
        }
        Node<T> newTail = tail.getPrev();
        tail.setPrev(null);
        newTail.setNext(null);
        tail = newTail;

        size--;
    }

    public void removeFirst() {
        if (size < 1) {
            throw new IndexOutOfBoundsException();
        }
        if (size == 1) {
            removeOnlyElement();
            return;
        }
        Node<T> newHead = head.getNext();
        head.setNext(null);
        newHead.setPrev(null);
        head = newHead;

        size--;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");

        Node<T> tmp = head;
        while (tmp != null) {
            sb.append(tmp.getData());
            sb.append(", ");
            tmp = tmp.getNext();
        }
        // Efekt -> [1, 2, 3, 4, 5, 6, ] <- trzeba usunąć przecinek i spację na końcu (', ')
        String result;
        if (size > 0) {
            result = sb.substring(0, sb.length() - 2); // usuwamy przecinek i spację na końcu
        } else {
            result = sb.toString();
        }
        result += "]";
        // [1, 2, 3, 4, 5, 6] <- efekt końcowy

        return result;
    }
}
